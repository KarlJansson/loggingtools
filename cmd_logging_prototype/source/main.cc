#include "log_writer.h"

int main(int count, char** vargs) {
  auto logger = LogWriter();
  for (auto i = 0; i < count; ++i) {
    if (std::string(vargs[i]).compare("-g") == 0)
      logger.GenerateLogFile(std::string(vargs[i + 1]),
                             std::stoi(vargs[i + 2]));
  }

  return 1;
}
