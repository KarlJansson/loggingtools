#pragma once
#include <string>
#include <unordered_map>
#include <vector>

class LogWriter {
 public:
  void GenerateLogFile(const std::string& out_file, const size_t lines);
  void GenerateLogDatabase(const std::string& out_file, const size_t lines);

  void ReadLogDatabase(const std::string& in_file);
  void DecodeLogFile(const std::string& in_file, const std::string& out_file);
  void EncodeLogFile(const std::string& in_file, const std::string& out_file);

 private:
  void DecompressMemory(std::vector<uint8_t>& in_data,
                        std::vector<uint8_t>& out_data);
  void CompressMemory(std::vector<uint8_t>& in_data,
                      std::vector<uint8_t>& out_data);

  std::unordered_map<std::string, const size_t> trace_string_map_;
  std::unordered_map<size_t, const std::string> trace_hash_map_;
  std::vector<std::pair<std::string, size_t>> trace_strings_;
};
