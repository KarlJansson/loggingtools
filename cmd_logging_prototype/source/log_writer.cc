#include "log_writer.h"
#include <zlib.h>
#include <chrono>
#include <fstream>
#include <random>

void LogWriter::GenerateLogFile(const std::string& out_file,
                                const size_t lines) {
  GenerateLogDatabase(out_file, lines);

  std::random_device rng;
  std::uniform_int_distribution<size_t> log_id(0, trace_strings_.size() - 1);

  std::vector<uint8_t> raw_data;
  std::ofstream open_txt(out_file + ".txt");
  for (auto i = 0; i < lines; ++i) {
    auto rnd = log_id(rng);
    for (auto ii = 0; ii < sizeof(trace_strings_[rnd].second); ++ii)
      raw_data.push_back(
          reinterpret_cast<uint8_t*>(&trace_strings_[rnd].second)[ii]);
    open_txt << trace_strings_[rnd].first << "\n";
  }

  std::vector<uint8_t> compressed_data;
  CompressMemory(raw_data, compressed_data);

  std::ofstream open(out_file + ".encode", std::ios::binary);
  open.write((char*)compressed_data.data(), compressed_data.size());
}

void LogWriter::GenerateLogDatabase(const std::string& out_file,
                                    const size_t lines) {
  std::vector<uint8_t> raw_data;

  trace_hash_map_.clear();
  trace_string_map_.clear();

  const char char_table[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
                             'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
                             's', 't', 'u', 'v', 'x', 'y', 'z'};

  std::random_device rng;
  std::uniform_int_distribution<size_t> line_length(69, 300);
  std::uniform_int_distribution<size_t> word_length(3, 15);
  std::uniform_int_distribution<size_t> char_length(0, 24);

  std::string line_str;
  for (auto i = 0; i < lines; ++i) {
    line_str = "";

    auto line = line_length(rng);
    auto word = word_length(rng);
    while (--line != 0) {
      line_str += char_table[char_length(rng)];

      if (--word == 0) {
        line_str += " ";
        word = word_length(rng);
      }
    }

    size_t hash = std::hash<std::string>{}(line_str);
    if (auto it = trace_hash_map_.find(hash); it == trace_hash_map_.end()) {
      trace_string_map_.insert({line_str, hash});
      trace_hash_map_.insert({hash, line_str});
      trace_strings_.push_back({line_str, hash});

      for (auto ii = 0; ii < sizeof(hash); ++ii)
        raw_data.push_back(reinterpret_cast<uint8_t*>(&hash)[ii]);
      for (auto ii = 0; ii < line_str.size(); ++ii)
        raw_data.push_back(line_str[ii]);
    }
  }

  std::vector<uint8_t> compressed_data;
  CompressMemory(raw_data, compressed_data);

  std::ofstream open(out_file + "_logdb", std::ios::binary);
  open.write((char*)compressed_data.data(), compressed_data.size());
}

void LogWriter::ReadLogDatabase(const std::string& in_file) {}

void LogWriter::DecodeLogFile(const std::string& in_file,
                              const std::string& out_file) {
  std::ifstream open(in_file);
  if (!open.fail()) {
  }
}

void LogWriter::EncodeLogFile(const std::string& in_file,
                              const std::string& out_file) {
  std::ifstream open(in_file);
  if (!open.fail()) {
  }
}

void LogWriter::CompressMemory(std::vector<uint8_t>& in_data,
                               std::vector<uint8_t>& out_data) {
  std::vector<uint8_t> buffer;
  buffer.reserve(in_data.size());
  const size_t BUFFSIZE = 128 * 1024;
  std::vector<uint8_t> temp_buffer(BUFFSIZE);

  z_stream strm;
  strm.zalloc = nullptr;
  strm.zfree = nullptr;
  strm.next_in = in_data.data();
  strm.avail_in = uInt(in_data.size());
  strm.next_out = temp_buffer.data();
  strm.avail_out = BUFFSIZE;

  deflateInit(&strm, Z_BEST_COMPRESSION);

  while (strm.avail_in != 0) {
    deflate(&strm, Z_NO_FLUSH);
    if (strm.avail_out == 0) {
      buffer.insert(buffer.end(), temp_buffer.begin(), temp_buffer.end());
      strm.next_out = temp_buffer.data();
      strm.avail_out = BUFFSIZE;
    }
  }

  int deflate_res = Z_OK;
  while (deflate_res == Z_OK) {
    if (strm.avail_out == 0) {
      buffer.insert(buffer.end(), temp_buffer.begin(), temp_buffer.end());
      strm.next_out = temp_buffer.data();
      strm.avail_out = BUFFSIZE;
    }
    deflate_res = deflate(&strm, Z_FINISH);
  }

  buffer.insert(buffer.end(), temp_buffer.begin(),
                temp_buffer.end() - strm.avail_out);
  deflateEnd(&strm);

  buffer.shrink_to_fit();
  out_data.swap(buffer);
}

void LogWriter::DecompressMemory(std::vector<uint8_t>& in_data,
                                 std::vector<uint8_t>& out_data) {
  std::vector<uint8_t> buffer;
  buffer.reserve(in_data.size() * 2);
  const size_t BUFFSIZE = 128 * 1024;
  std::vector<uint8_t> temp_buffer(BUFFSIZE);

  z_stream strm;
  strm.opaque = nullptr;
  strm.zalloc = nullptr;
  strm.zfree = nullptr;
  strm.next_in = in_data.data();
  strm.avail_in = uInt(in_data.size());
  strm.next_out = temp_buffer.data();
  strm.avail_out = BUFFSIZE;

  inflateInit(&strm);

  int res = Z_OK;
  while (res != Z_STREAM_END) {
    res = inflate(&strm, Z_NO_FLUSH);
    if (strm.avail_out == 0) {
      buffer.insert(buffer.end(), temp_buffer.begin(), temp_buffer.end());
      strm.next_out = temp_buffer.data();
      strm.avail_out = BUFFSIZE;
    }
  }

  buffer.insert(buffer.end(), temp_buffer.begin(),
                temp_buffer.end() - strm.avail_out);
  inflateEnd(&strm);

  buffer.shrink_to_fit();
  out_data.swap(buffer);
}
